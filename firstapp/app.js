// function sayHello(name){
//     console.log("Hello " + name)
// }

// // sayHello('albin')
// // console.log(window)
// // console.log(); // global
// // setTimeout(() => {

// // }, 23);
// // clearTimeout()
// // setInterval()
// // clearInterval()

// // var message = ""
// // console.log(global.message)
// // node does not have window. its called global

// const logger = require('./logger.js')

// console.log(logger.log("wow"))

// const path = require("path")

// var pthobj = path.parse(__filename);
// console.log(pthobj)

// const os = require("os")
// var totalMemory = os.totalmem();
// var freeMemory = os.freemem();

// console.log(`Total Mem = ${totalMemory} and Free Mem = ${freeMemory}`);

// const fs = require("fs");

// const files = fs.readdirSync("./")
// console.log(files)

// fs.readdir("./", function(error, data){
//     if(error) console.log("error", error);
//     else console.log("result", data);

// })

// // this is a class so you need an object
// const EventEmitter = require("events");
// const emitter = new EventEmitter();
// //register a listener
// emitter.on('messageLogged', (arg) => {
//     console.log("Listner called", arg);
// })
// //raise an event
// emitter.emit('messageLogged', { id : 1, url : "http://"});

// // raise an event called logging(data)

// emitter.on("Logging", (args) => {
// console.log(args);
// })

// emitter.emit("Logging", {user : "albin", time : new Date().getTime()})

// emitter.on("Logger", (args) => {
//     console.log("This is from logger " + args);
// })

// const Logger = require("./logger")
// // log.log("This is not send to on");

// const logger = new Logger();

// logger.on("messageLogger", (args) => {
//     console.log("Calling from class " + args);
// })


const http = require("http");
// const server = http.createServer();
// server.on('connection', (socket)=>{
//     console.log("New Connection", socket);
// })

// server.listen(3000);
// console.log("Listening on port 3000");


const server = http.createServer((req, res) => {
    if(req.url === "/"){
        res.write("Hello from server side");
        res.end();
    }

    if(req.url === "/api/courses"){
        res.write(JSON.stringify([1,2,3,4,5]));
        res.end();
    }
})

//this keeps the connection open
server.listen(3000);
console.log("Listening on port 3000");
