const EventEmitter = require("events")
const emitter = new EventEmitter()

var url = 'http://mylogger.io/log';

// this needs to be public so it can be exported to another app
function log(message){
    //send an http request
    console.log(message)

    emitter.emit("Logger", {message : message});
}

// module.exports.log = log;

class Logger extends EventEmitter {
    log(message) {
        console.log(message);
        //raise an event
        this.emit("messageLogger", {message : message});
    }
}

module.exports.Loggers = Logger