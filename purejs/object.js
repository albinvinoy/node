function Course(title, level, views){
    this.title = title;
    this.level = level;
    this.views = views;
    
    this.updateViews = function(){
        return ++this.views;
    }
}

var courses = [new Course("a", true, 2323), new Course("b", false, 23)];

// course1.updateViews()
console.log(courses[1].updateViews());