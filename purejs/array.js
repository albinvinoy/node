var pens;

pens = ["Red", "blue", "green", "orange", 1, true];

console.log(pens)
console.log(pens[3])
pens[0] = "red"

//properties
console.log("array.length = ", pens.length);

pens.reverse();
pens.shift(); // remove array head
pens.unshift("insert1", "insert2") // add to front of array
pens.pop(); // remove last item
pens.push("end1", "end2"); //add to the end of array
pens.splice(2,1); // start at 0, remove 2nd item 1 time

console.log(pens);

var newPens = pens.slice() // 0 based, (start at index, end at index) return section of array

var search = pens.indexOf("orange", 2); // return index number of 'orange' from index 2

console.log(pens.join(", ")); // take the list and return string with , after each item

