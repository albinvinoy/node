const MYCONSTANT = 5;
console.log(MYCONSTANT);

function logScope(){
    var localVar = 2;

    if(localVar){
        // var localVar = "Im different"; // this will rename every localVar
        let localVar = "Im different";
        console.log("nestedLocaVar : ", localVar);
    }

    console.log("localVar logScope : ", localVar);
}
logScope()