// document.querySelector(".class").classList;
// document.querySelector(".class").classList.add("newclass");
// document.querySelector(".class").classList.remove("newclass");
// document.querySelector(".class").classList.toggle("newclass");
// document.querySelector(".class").classList.contains("newclass");

document.body.innerHTML = '<a  href="https://www.google.com" target="_blank"> Book Now </a>'

const bookNow = document.querySelector("a");
if (bookNow.hasAttribute("target") && (bookNow.getAttribute("href") == "https://www.google.com")){
    console.log(bookNow)
}else{
    bookNow.setAttribute("target", "_blank");
    bookNow.setAttribute("href", "https://www.google.com")
}

var captionElement = document.createElement("figcaption");
var captionText = document.createTextNode("New Image here");

captionElement.appendChild(captionText);

console.log(captionElement)

document.body.appendChild(captionElement).style.backgroundColor = "green";