// regular function

// function multiply(a,b) {
//     console.log(multiply)
//     return a*b;
// }

// // multiply(1,4)

// var multi = function(a,b) {
//     console.log(a*b)
//     return a*b;
// }


// (function(a,b) {
//     console.log(a*b)
//     return a*b;
// })

//******************* */

// function findBiggestFraction(as, bs){
//     var result = {};
//     as > bs ? result = { "name": "as", "value": as } : result = { "name": "bs", "value": bs };
//     return result;
// }

// var bf = findBiggestFraction(13/5, 54/6);


//************************* */

// var theBiggest = function(as, bs){
//     var result = {};
//     as > bs ? result = { "name": "as", "value": as } : result = { "name": "bs", "value": bs };
//     return result;
// }

// console.log(theBiggest(7/9, 13/25));
// console.log(theBiggest);


//************************* */
// immediate invoke
var theBiggest = (function(as, bs){
    var result = {};
    as > bs ? result = { "name": "as", "value": as } : result = { "name": "bs", "value": bs };
    return result;
})(7/9, 23/12)

console.log(theBiggest);

